import React from 'react';
import ReactDOM from 'react-dom';
import styles from './estilos.module.css'; 

const Header = (props) => {

    return (
        <div>
			<h1 className={styles.header}>{props.course}</h1>
		</div>
    )
  };

export default Header;