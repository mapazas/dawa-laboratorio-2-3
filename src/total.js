import React from 'react';
import ReactDOM from 'react-dom';
import styles from './estilos.module.css'; 

const Total = (props) => {
	
	/* Se utiliza map() y  contador que permita recorrer todos los elementos del array y sumarlos
	para hallar el total de ejercicios */

	var total = 0
	props.parts.map(function(cont) {
		
		total = total + cont.exercises 
		
	})

    return (
		<div className={styles.total}>
			<p>Number of exercises: {total} </p>
		</div>
    )
  };

export default Total;